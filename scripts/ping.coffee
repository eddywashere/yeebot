# Description:
#   Utility commands surrounding Hubot uptime.
#
# Commands:
#   hubot ping - Reply with pong
#   hubot echo <text> - Reply back with <text>
#   hubot time - Reply with current time
#   hubot die - End hubot process

spawn = require('child_process').spawn

module.exports = (robot) ->
  robot.respond /PING$/i, (msg) ->
    msg.send "PONG"

  robot.respond /ECHO (.*)$/i, (msg) ->
    msg.send msg.match[1]

  robot.respond /TIME$/i, (msg) ->
    msg.send "Server time is: #{new Date()}"

  robot.respond /DIE$/i, (msg) ->
    msg.send "Goodbye, cruel world."
    process.exit 0

  robot.respond /MARCO$/i, (msg) ->
    child = spawn('/bin/sh', ['-c', "ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"])

    child.stdout.on 'data', (data) ->
      msg.send "POLO!!! #{data.toString().trim()}"
      child.stdin.end()